const flatten = require('../flatten');
const nestedArray = [0, 1, null, 2, [[[3, 4]]]] ;

console.log(flatten(nestedArray, 2));
// console.log(nestedArray.flat(Infinity))

//inputs 1 = [0, 1, 2, [[[3, 4]]]]
//inputs 2 = [1, 2, [3, 4, [5, 6]]]
//inputs 3 =  [1, 2, [3, 4, [5, 6, [7, 8, [9, 10]]]]]

//output 1 = [ 0, 1, 2, 3, 4 ]
//output 2 = [ 1, 2, 3, 4, 5, 6 ]
//output 3 = [1, 2, 3, 4,  5, 6, 7, 8, 9, 10]