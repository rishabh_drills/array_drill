const map = require('../map');
const items = [1, 2, 3, 4, 5, 5];
const cb = (element, index, elements) => {

    return element * 2;
}
// let result = map(elements[i]);
console.log(map(items, cb));

//element / 2 = [ 0.5, 1, 1.5, 2, 2.5, 2.5 ]
//element * 1.5 =  [ 1.5, 3, 4.5, 6, 7.5, 7.5 ]
//element % 3 = [ 1, 2, 0, 1, 2, 2 ]