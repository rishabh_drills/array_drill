function flatten(elements,depth) 
{   
    if(depth === 0)
    {
        return elements;
    }

    if(elements.length===0)
    {
        return [];
    }

    if(!depth){

        depth = 1;
    }
    
    let flatArr = []; 

    for(let index = 0; index < elements.length; index++)
    {
        if(Array.isArray(elements[index]) && depth >= 1)
        { 
            flatArr = flatArr.concat(flatten(elements[index],depth-1));
            
        }else if(elements[index] === undefined || elements[index] === null){
            continue;
        }

        else { 
            flatArr.push(elements[index]);
        }
 
    }

    return flatArr
}

module.exports = flatten;