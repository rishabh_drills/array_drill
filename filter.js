function filter(elements=[], cb) 
{
    let newArray = [];
    if(elements.length === 0)
    {
        return [];
    }
    for(let index = 0; index < elements.length; index++)
    {
        // let newArray = [];
        if(cb(elements[index], index, elements) == true)
        {
            newArray.push(elements[index]);
        }
        // return newArr;
    }
    return newArray;
}

module.exports = filter;