function find(elements = [], cb) 
{
    if(elements.length === 0)
    {
        return;
    }
    for(let index = 0;index < elements.length; index++)
    {
        let truthTest = cb(elements[index], index, elements);
        // console.log(truthTest);
        if(truthTest)
        {
            return elements[index];
        
        }
    }
    // return;
}

module.exports = find;
