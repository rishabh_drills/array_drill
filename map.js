function map(elements, cb)
{
    if(elements.length === 0)
    {
        return [];
    }
    let output = [];
    for(let index = 0; index < elements.length; index++)
    {
        // let output = [];
        
        let cbArray = cb(elements[index], index, elements);
        output.push(cbArray);
    }
    return output;
}

module.exports = map;